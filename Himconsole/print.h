// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#include "color.h"
#include "type.h"

#ifndef PRINT_H_
#define PRINT_H_


namespace print
{

void info(string);
void good(string);
void err(string);
void warn(string);
void excp(const char* file, const char* func, int line);

}	// namespace print


#endif	// PRINT_H_
