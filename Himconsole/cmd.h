// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian
// ����̨����

#ifndef CMD_H_
#define CMD_H_

namespace cmd
{

void console();

void banner();
void clear();
void db();
void exit();
void help();
void history();
void listen();
void ls();
void use();

}	// namespace cmd


#endif	// CMD_H_
