// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#include "socket.h"
#include "print.h"

uint Socket::size_ = 0;

Socket::Socket()
{
	size_++;

	// 如果为第一个实例
	if(size_ == 1)	// 初始化WSA
	{
		WSADATA	WSAData;
		WSAStartup(MAKEWORD(2, 2), &WSAData);
	}
}

Socket::~Socket()
{
	size_--;

	// 如果没有实例
	if (size_ == 0)	// WSA释放
		WSACleanup();
}
